# EthKeyscape

EthKeyscape acts as a proof-by-attack of the security of the Ethereum Keyspace.
Skip the story and get to the good part HERE [link to finished demo]

### Backstory

I have had Ethereum on my mind off and on since the first papers were released.  
Sadly I didn't have the foresight or capital to become rich from it when it was easier, in the early days.
I like to chew on mathematics problems, and I was thinking about all the private wallets people had generated around the world.
Having used MyEtherWallet (100% love there too) I was curious about the 'password' feature.
If people generated their keys based on a simple password, it should be possible to reproduce their secret keys.
I thought it would be interesting to have a big database of keys.
Lucky for me, living in the internet future means there's already *more than one!*
~link to the simple one, and pic
That's just text, and not really very zazzy.
This next one has a feature I love: ethscan integration. It makes it like a slot machine.
~link and pic to better one
Sure, the win rate is %0.000...1, but the potential payout is MILLIONS OF DOLLARS and the cost to spin is ~free!
So, naturally, I wanted to spin infinite times!
I wrote this quick script with the help of npm (hearts!) and the etherscan api.
//script block and links, pics

I feel really good about it. I feel good about all my free spins, and I feel good that I haven't had any hits.
Run it yourself, and I bet you won't have any hits either! 

So I like that. But it's not really cool enough, you know? If you have to explain it so much, it's not really accessible.
I wanted people to get a feeling of the size of the keyspace.
One of my great loves in this life is FRACTALS.
A classic fractal is the Hilbert Curve. I wanted to use the mapping properties of the Hilbert curve to show how private keys compute to public keys.
I got lucky that threejs had a demo that included a hilbert curve.
I love the free software future!

After wrestling with vectors and matrices and the threejs docs, examples, and source...
I was able to create a cool-looking mapping!
I wonder if showing some progress is in order?
There's the unit curve.
There's the mapping from one unit curve to another.
The next order curve, with a single mapping.
The next. 
And the next. Rendering gets choppy, so we don't go this high.
In order to represent the keyspace 1:1 would require incredible resources.
I use a simplified mapping that discards most of the bits of the key, but works.
This is just the visualization anyway - the real work of checking actual keys and addresses is just being represented graphically.

Goal: Private Keyspace, Public Keyspace, Public Addressspace, 100 links at >1/sec
sweet text overlay, mobile impressive, desktop jokes background performance


Another element of this story is QR codes.  
My friend and I spent an evening trying our hardest to buy some ETH,
and we found it really difficult.
I was fantasizing about making it easy by selling people 'plastic ingots',
or basically 3d printed QR code packages with private key and public address.
Then I was thinking it would be really cool to give vanity addresses out, 
and another avenue of research was embarked upon.
I remembered something about parity bits and I went out searching for the truth.
Would I have to brute force the generation of vanity addresses? Sort of yes.


So it goes like this:
```mermaid
graph LR;
  A(Random number generator)-->B{Private Key}
  subgraph Ethereum Key Space
  B-->C[Public Key]
  B-.Controls.->D((Public Address))
  C-->D
  end
```
or with more verbs
```mermaid
graph LR;
  A(Random number generator)--generates-->B{Private Key}
  subgraph Ethereum Key Space
  B--hashes to-->C[Public Key]
  B-.Controls.->D((Public Address))
  C--rounds to-->D
  end
```
That graphic is thanks to https://mermaidjs.github.io/ ! Cool stuff!
The live editor really shines: https://mermaidjs.github.io/mermaid-live-editor/

Okay now that I have played with mermaid a little, I'm feeling the limitations.
Nice and quick and fun to start, weak soon after. Subgraph nesting is not great.

```mermaid
graph LR;
  subgraph Local Computation
  A(Random number generator)--generates-->B{Private Key}
    subgraph Ethereum Key Space
    B--hashes to-->C[Public Key]
    B-.Controls.->D((Public Address))
    C--rounds to-->D
    end
  end
  subgraph Live Ethereum Network
  D-->E[MyEtherWallet API to online Nodes]
  end
```
That's fine, we'll stick with small modules. It'll be okay. :+1:
As an aside thought, I'll be sure to get the rendering pipeline and save some generated HTML for my pages. 
I'd hate to think I might lose my presentation in the long tail of time.

SO,
QR codes for public addresses.

In order to get specific QR codes, you need specific addresses.
But QR codes have features that give us some more pixels to work with.

Sorry, I got too interested in mermaid, and now I'm looking at graphviz and reading the mermaid source code.
Maybe if I'm lucky I can add the subgraph features I want, for a great public victory.
Exciting!
Okay, so reading the issues, it looks like namespacing fixes some problems.

```mermaid
graph LR;
  subgraph Local Computation
  L.A(Random number generator)--generates-->L.B{Private Key}
 L.E-->L.F(THREEjs Render)
    subgraph Ethereum Key Space
    L.B--hashes to-->L.C[Public Key]
    L.B-.Controls.->L.D((Public Address))
    L.C--rounds to-->L.D
    end
  end
  subgraph Live Ethereum Network
  L.D-->E.E
  E.E-->L.D
  E.D((Public Address))===E.E[MyEtherWallet API to online Nodes]
  end
```

Getting the hang of it.

```mermaid
graph LR;
  subgraph Local Computation
  L.A(Random number generator)--generates-->L.B{Private Key}
 L.E-->L.F(THREEjs Render)
    subgraph Ethereum Key Space
    L.B--hashes to-->L.C[Public Key]
    L.B-.Controls.->L.D((Public Address))
    L.C--rounds to-->L.D
    end
  end
  subgraph Live Ethereum Network
  L.D-->E.E
  E.E-->L.D
  E.E[MyEtherWallet API to online Nodes]===E.D((Public Address))
  end
```

Aw jeeze, digging into it a little more, the project doesn't have double sided or reversible arrows, it seems. https://github.com/knsv/mermaid/issues/123
I don't know if I'm up for a feature of this magnitude, since it will surely involve a huge and fiendish stack of technologies...
But it seems exciting. Let's see how far we get in a single stab.
Ah yes, the creator speaks: "I am afraid that Mermaid is not very expressive this way."
https://github.com/knsv/mermaid/issues/287
Let's keep digging - go straight to the source :)

So this is a language being turned into svg for display.
Let's look for the parser. mermaid.js makes me think it's at mermaidAPI.render
import graph from './diagrams/flowchart/graphDb'
./diagrams/flowchart yeilds /parser! getting closer, friends!

hm linkStyle 0 stroke-width:9px back pocket

New discovery: JISON. Is it gonna be tasty?

I guess the trick here is to enable discrete subgraphs as entities.
Or... implement reverse-direction-arrow first. Let's try that.	:)

Let's learn JISON for a bit! http://zaa.ch/jison/docs/

This is how you get good research done - getting FAR off the beaten track.
Rememember how this was about Ethereum Keys?
It'll come full circle, but first we'll find out how many level we get through first.
So, JISON.

Okay, and to learn the basic concets, it's BISON http://dinosaur.compilertools.net/bison/bison_4.html#SEC7
Great! 
...
Wooo! lexing! parsers! getting elbow deep in regular expressions!
So https://github.com/knsv/mermaid/blob/master/src/diagrams/flowchart/parser/flow.jison 
that makes some more sense now.
So to define a reverse arrow, I'll need to add that to the lexical grammar
Which regexp is this again? Gonna have to dig that up. No mistakes allowed.
Hopefull I can get some nice regexp compilation going. Another layer!

So a new graphCodeTokens entry as well. A few changes at once.

It looks like  | '--' text ARROW_CROSS
{$$ = {"type":"arrow_cross","stroke":"normal","text":$2};}
arrow cross (--x) doesn't do anything special and isn't used.
Let's try to repurpose it for our double pointy arrow.
I guess I'll need to find a style for it. Where? SVG? CSS? lol both.
https://svgwg.org/specs/markers/
Progress! marker-start="url(#arrowhead1087)" and marker-end="url(#arrowhead1087)" and maybe 'auto-start-reverse'
When added to the live editor's output arrow using browser dev tools,
this make a double sided arrow. (ref: g.edgepath path)
So it seems most of the things we need are already close.
https://github.com/knsv/mermaid/blob/master/src/diagrams/flowchart/flowRenderer.js
'marker'
dagreD3.render
The rabbit hooooooole
I'm hungry, taking a break.

So, after a mouthful of spinach and tree nuts, the quest continues.
dagreD3.render means we're looking at D3 for the actual rendering, 
and dagre is for directed graphs, and is new to me. let's look it up!

Ahhhhhhh now things are becoming clearer.
https://dagrejs.github.io/project/dagre-d3/latest/demo/arrows.html
The REASON that there aren't double-sided arrows is because,
mermaid is built on dagre-d3, and dagre itself was made for
DIRECTED GRAPHS, which don't need double sided arrows. Neat.

https://dagrejs.github.io/project/dagre-d3/latest/demo/user-defined.html
I found this, and also: http://www.webgraphviz.com/

This makes me consider whether or not this really matters at all.
AHEM.

I took a break, and when I came back, I saw that I was working on ...
too many things at once. I'm glad I learned about JISON, that might come in handy.
And I'm glad I learned about mermaid, and webgraphviz.
But that wasn't what this was really supposed to be about!  

## Return to EthKeyScape

We've got:
 - Keywhacking
 - Vizualizing
 - Vanity QR codes for 3D printing

I'm not 100% done with this mermaid stuff - I'm checking on https://gitlab.com/gitlab-org/gitlab-ce/commit/13ba6966e21ee8362a025e46feb1ed3e86fadeb6
making sure I can generate these pages to serve on my own.
It seems a little bit important.
A bit of Ruby never hurt nobody.  
Okay, it looks like I'm going to save installing a local copy of Gitlab-CE for later.
Let's knock out this ETH project visualization 
so people can run it on their phones and in their browsers
and exclaim "whoaaa!"
No more log writing for today.
